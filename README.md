# Randomize MAC Addresses on a GNU/Linux-based OS
An experimental and simple bash script to randomize MAC address to avoid tracking on public APs, that you can either execute manually, after login, or using cronjobs…


## How to use
Check your interfaces names, if the one you want to change the MAC address for is "wlan0", then the script should work out of the box. If the name is different, you have to replace "wlan0" by the correct name, in the script ("iface" value). I will eventually automate this some day…

Put the script and *mac-addr* text file (containing 8663 OUIs) in the same directory, make the script executable, and execute the script from you terminal (or automagically) as root (required to set a MAC address). Changing directory to the script's directory is *not* required. You can execute the script from anywhere (using full path or $PATH environment variable) since the path to *mac-addr* is absolute/appended dynamically at execution time, using the BASH_SOURCE array.


## OS Support
The current version works GNU/Linux distros that have openssl and iproute2 installed. Even though I tested it only on Debian (which is my main/only OS), It should work with other distros, including Red-Hat based ones, but you might need to change the interface's name ("iface" variable's value) for some distros.

However, It **does not** support *BSDs since the scripts uses iproute2 instead of net-tools (deprecated in Linux-based distros) and randomization depend on OpenSSL. BSDs and LibreSSL-based OSes will be provided someday. Until then, feel free to modify the scrip according to your needs.


## Dependencies
- openssl (for randomization)
- iproute2


## Warning/To-do list
The current version works but does not log errors (if any) and lacks some features. Thus, I considered it to experimental until I add some code to
- Detect/log errors and correct them on the fly (when possible). So if you wish to use this script at boot time, test it on your system first.
- Automagically detect interfaces names by parsing ip (or ifconfig) output
- Enable bash options support to specify interfaces names. Currently it's stored as a variable value.
- Make it possible to randomize MAC addresses for multiple interfaces at once (loop n times where n = number of interfaces, based on user choice).

Unfortunately, I currently lack time to add these features but wanted to make this script any way, even a basic version of it. Otherwise I know I won't do shit for a long time…


## Why don't you just use macchanger or $stuff?
Yes, I am aware about macchanger, but I wanted something more basic. And I also wanted to find a way to do it myself, just for fun.

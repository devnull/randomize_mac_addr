#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILE="$SCRIPT_DIR/mac-addr"
iface="wlan0"
randmacaddr="$(shuf -n 1 "$FILE"):$(openssl rand -hex 3 | tr 'a-z' 'A-Z' | sed -e 's/../:&/2g')"
echo "$randmacaddr"

ip link set dev "$iface" down
ip link set dev "$iface" address "$randmacaddr"
ip link set dev "$iface" up

exit 0